package cz.fely.bakaapi.v3.models

import cz.fely.bakaapi.v3.ext.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class Substitutions(
    @Serializable(DateSerializer::class) @SerialName("From") val from: Date,
    @Serializable(DateSerializer::class) @SerialName("To") val to: Date,
    @SerialName("Changes") val changes: List<Change>
): IBakaResult