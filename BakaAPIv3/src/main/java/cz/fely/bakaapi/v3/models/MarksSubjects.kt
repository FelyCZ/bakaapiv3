package cz.fely.bakaapi.v3.models

import cz.fely.bakaapi.v3.ext.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonArraySerializer
import java.util.*

@Serializable
data class MarksSubjects(
    @SerialName("Subjects") val subjects: List<MarksSubject>
) : IBakaResult {
    @Serializable
    data class MarksSubject(
        @SerialName("Subject") val subjectInfo: SubjectInfo,
        @SerialName("AverageText") val average: String,
        @SerialName("TemporaryMark") val temporaryMark: String?,
        @SerialName("SubjectNote") val subjectNote: String?,
        @SerialName("TemporaryMarkNote") val temporaryMarkNote: String?,
        @SerialName("PointsOnly") val pointsOnly: Boolean,
        @SerialName("MarkPredictionEnabled") val markPredictionEnabled: Boolean,
        /*@Serializable(JsonArraySerializer::class)*/ @SerialName("Marks") val marks: List<Mark>
    ) {
        @Serializable
        data class Mark(
            @Serializable(DateSerializer::class) @SerialName("MarkDate") val markDate: Date,
            @Serializable(DateSerializer::class) @SerialName("EditDate") val editDate: Date,
            @SerialName("Caption") val caption: String,
            @SerialName("Theme") val theme: String? = null,
            @SerialName("MarkText") val mark: String,
            @SerialName("TeacherId") val teacherId: String,
            @SerialName("Type") val type: String? = null,
            @SerialName("TypeNote") val typeNote: String? = null,
            @SerialName("Weight") val weight: Int? = null,
            @SerialName("SubjectId") val subjectId: String,
            @SerialName("IsNew") val isNew: Boolean,
            @SerialName("IsPoints") val isPoints: Boolean,
            @SerialName("CalculatedMarkText") val calculatedMark: String? = null,
            @SerialName("ClassRankText") val classRank: String? = null,
            @SerialName("Id") val id: String,
            @SerialName("PointsText") val points: String? = null,
            @SerialName("MaxPoints") val maxPoints: Int
        )
    }
}