package cz.fely.bakaapi.v3

import android.content.ContentValues.TAG
import android.util.Log
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.serialization.responseObject
import cz.fely.bakaapi.v3.exceptions.BakaException
import cz.fely.bakaapi.v3.models.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

@OptIn(ImplicitReflectionSerializer::class)
fun BakaUser.login(): BakaResult<Login, Exception> {
    val params = listOf(
        "client_id" to "ANDR",
        "grant_type" to "password",
        "username" to username,
        "password" to password
    )

    val (request, response, result) = "$url/api/login".httpPost(params).responseObject<Login>()
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            if (result.value.error.isNullOrEmpty()) {
                BakaResult.Failure(result.value, BakaException("login failed (see result error)"))
            }
            BakaResult.Success(result.value)
        }
    }
}

@UnstableDefault
@OptIn(ImplicitReflectionSerializer::class)
fun BakaUser.getUserInfo(): BakaResult<UserInfo, Exception> {

    val (request, response, result) = "$url/api/3/user".httpGet().authentication()
        .bearer(accessToken)
        .responseObject<UserInfo>(Json { isLenient = true; ignoreUnknownKeys = true })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getMarks(): BakaResult<MarksSubjects, Exception> {

    val (request, response, result) = "$url/api/3/marks".httpGet().authentication()
        .bearer(accessToken)
        .responseObject<MarksSubjects>(Json { isLenient = true; ignoreUnknownKeys = true })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getFinalMarks(): BakaResult<FinalMarks, Exception> {

    val (request, response, result) = "$url/api/3/marks/final".httpGet().authentication().bearer(accessToken)
        .responseObject<FinalMarks>(Json {isLenient = true; ignoreUnknownKeys = true})
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getMarksMeasures(): BakaResult<MarkMeasure, Exception> {

    val (request, response, result) = "$url/api/3/marks/final".httpGet().authentication().bearer(accessToken)
        .responseObject<MarkMeasure>(Json {isLenient = true; ignoreUnknownKeys = true})
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}



@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getTimetable(): BakaResult<Timetable, Exception> {

    val (request, response, result) = "$url/api/3/timetable/permanent".httpGet().authentication()
        .bearer(accessToken)
        .responseObject<Timetable>(Json {
            isLenient = true; ignoreUnknownKeys = true; encodeDefaults = false
        })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getTimetable(date: Date = Date()): BakaResult<Timetable, Exception> {

    val dateS = SimpleDateFormat("YYYY-MM-dd", Locale.getDefault()).format(date)

    val (request, response, result) = "$url/api/3/timetable/actual?date=$dateS".httpGet()
        .authentication().bearer(accessToken)
        .responseObject<Timetable>(Json { isLenient = true; ignoreUnknownKeys = true })
    Log.d(TAG, "getTimetable: ${request.url}")
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getSubjects(): BakaResult<Subjects, Exception> {

    val (request, response, result) = "$url/api/3/subjects".httpGet().authentication()
        .bearer(accessToken)
        .responseObject<Subjects>(Json { isLenient = true; ignoreUnknownKeys = true })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getThemes(): BakaResult<Themes, Exception> {

    val (request, response, result) = "$url/api/3/subjects".httpGet().authentication()
        .bearer(accessToken)
        .responseObject<Themes>(Json { isLenient = true; ignoreUnknownKeys = true })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getApiInfo(): BakaResult<ApiInfo, Exception> {

    val (request, response, result) = "$url/api".httpGet().authentication().bearer(accessToken)
        .responseObject<ApiInfo>(Json { isLenient = true; ignoreUnknownKeys = true })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getApiInfo3(): BakaResult<ApiInfo, Exception> {

    val (request, response, result) = "$url/api/3".httpGet().authentication().bearer(accessToken)
        .responseObject<ApiInfo>(Json { isLenient = true; ignoreUnknownKeys = true })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getAbsence(): BakaResult<StudentAbsence, Exception> {

    val (request, response, result) = "$url/api/3".httpGet().authentication().bearer(accessToken)
        .responseObject<StudentAbsence>(Json { isLenient = true; ignoreUnknownKeys = true })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getSubstitutions(date: Date? = null): BakaResult<Substitutions, Exception> {

    val url2 = if(date != null) "$url/api/3/substitutions?${SimpleDateFormat("YYYY-MM-dd", Locale.getDefault()).format(date)}" else "$url/api/3/substitutions"
    val (request, response, result) = url2.httpGet().authentication().bearer(accessToken)
        .responseObject<Substitutions>(Json { isLenient = true; ignoreUnknownKeys = true })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}

@OptIn(ImplicitReflectionSerializer::class)
@UnstableDefault
fun BakaUser.getEvents(date: Date? = null): BakaResult<Events, Exception> {

    val url2 = if(date != null) "$url/api/3/substitutions?${SimpleDateFormat("YYYY-MM-dd", Locale.getDefault()).format(date)}" else "$url/api/3/substitutions"
    val (request, response, result) = url2.httpGet().authentication().bearer(accessToken)
        .responseObject<Events>(Json { isLenient = true; ignoreUnknownKeys = true })
    return when (result) {
        is Result.Failure -> {
            BakaResult.Failure(null, result.error)
        }
        is Result.Success -> {
            BakaResult.Success(result.value)
        }
    }
}