package cz.fely.bakaapi.v3.models

import cz.fely.bakaapi.v3.ext.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class StudentAbsence (
    @SerialName("PercentageThreshold") val percentageThreshold: Double,
    @SerialName("Absences") val absences: List<Absence>
): IBakaResult {
    @Serializable
    data class Absence(
        @Serializable(DateSerializer::class) @SerialName("Date") val date: Date,
        @SerialName("Unsolved") val unsolved: Int,
        @SerialName("Ok") val ok: Int,
        @SerialName("Missed") val missed: Int,
        @SerialName("Late") val late: Int,
        @SerialName("Soon") val soon: Int,
        @SerialName("School") val school: Int
    )

    @Serializable
    data class SubjectAbsence(
        @SerialName("SubjectName") val subjectName: String,
        @SerialName("LessonsCount") val lessonsCount: Int,
        @SerialName("Base") val base: Int,
        @SerialName("Late") val late: Int,
        @SerialName("Soon") val soon: Int,
        @SerialName("School") val school: Int
    )
}