package cz.fely.bakaapi.v3.models

import cz.fely.bakaapi.v3.ext.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class Themes(
    @SerialName("Subject") val subject: SubjectInfo,
    @SerialName("Themes") val themes: List<Theme>
): IBakaResult {

    @Serializable
    data class Theme(
        @Serializable(DateSerializer::class) @SerialName("Date") val date: Date,
        @SerialName("Theme") val theme: String,
        @SerialName("Note") val note: String,
        @SerialName("HourCaption") val hourCaption: String,
        @SerialName("LessonLabel") val lessonLabel: String
    )
}