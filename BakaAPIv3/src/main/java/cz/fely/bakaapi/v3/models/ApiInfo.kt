package cz.fely.bakaapi.v3.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ApiInfo(
    @SerialName("ApiVersion") val apiVersion: String,
    @SerialName("ApplicationVersion") val appVersion: String,
    @SerialName("BaseUrl") val baseUrl: String
): IBakaResult