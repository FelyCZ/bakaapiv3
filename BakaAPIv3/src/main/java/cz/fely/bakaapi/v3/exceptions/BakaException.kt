package cz.fely.bakaapi.v3.exceptions

import java.lang.Exception

class BakaException (override val message: String?, val responseCode: Int? = null) : Exception()