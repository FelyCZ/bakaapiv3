package cz.fely.bakaapi.v3.tester

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import cz.fely.bakaapi.v3.*
import cz.fely.bakaapi.v3.models.BakaResult
import cz.fely.bakaapi.v3.models.BakaUser
import org.jetbrains.anko.*
import java.util.*

@Suppress("EXPERIMENTAL_API_USAGE")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var b = BakaUser("https://bakalari.gypce.cz/bakaweb", "020928tptm", "57sj7puh")
        doAsync {
            val r1 = b.login().get()
            b.apply {
                this.accessToken = r1.accessToken ?: ""
                this.refreshToken = r1.refreshToken ?: ""
            }
            val r = b.getSubjects().fold({ result ->
                activityUiThreadWithContext {
                    longToast(result.subjects.toString())
                }
            }, { exception, result ->
                activityUiThreadWithContext {
                    Log.e("TAG", "onCreate: $exception" )
                }
            })

        }
    }
}