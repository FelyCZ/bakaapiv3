package cz.fely.bakaapi.v3.models

import kotlinx.serialization.Serializable

@Serializable
class BakaUser(
    val url: String,
    val username: String,
    val password: String,
    var accessToken: String = "",
    var refreshToken: String = ""
) {

}
