package cz.fely.bakaapi.v3.models

import cz.fely.bakaapi.v3.ext.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject
import java.util.*

@Serializable
data class Change(
    @SerialName("ChangeSubject") val changeSubject: JsonObject,
    @Serializable(DateSerializer::class) @SerialName("Day") val day: Date,
    @SerialName("Hours") val hours: String,
    @SerialName("ChangeType") val changeType: String,
    @SerialName("Description") val description: String,
    @SerialName("Time") val time: String,
    @SerialName("TypeAbbrev") val typeAbbreviation: String,
    @SerialName("TypeName") val typeName: String
)