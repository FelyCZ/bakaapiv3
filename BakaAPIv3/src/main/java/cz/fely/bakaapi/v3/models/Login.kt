package cz.fely.bakaapi.v3.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Login(
    @SerialName("access_token") val accessToken: String? = null,
    @SerialName("refresh_token") val refreshToken: String? = null,
    @SerialName("token_type") val tokenType: String? = null,
    @SerialName("expires_in") val expiresIn: Int? = null,
    @SerialName("bak:ApiVersion") val bakApiVersion: String? = null,
    @SerialName("bak:AppVersion") val bakAppVersion: String? = null,
    @SerialName("bak:UserId") val bakUserId: String? = null,
    @SerialName("error") val error: String? = null,
    @SerialName("error_description") val error_description: String? = null
): IBakaResult {

}