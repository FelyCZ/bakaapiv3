package cz.fely.bakaapi.v3.models

import kotlinx.serialization.SerialInfo
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject

@Serializable
data class UserInfo(
    @SerialName("UserUID") val userUID: String,
    @SerialName("Class") val clazz: JsonObject,
    @SerialName("FullName") val fullName: String,
    @SerialName("SchoolOrganizationName") val schoolOrganizationName: String,
    @SerialName("SchoolType") val schoolType: String? = null,
    @SerialName("UserType") val userType: String,
    @SerialName("UserTypeText") val userTypeText: String,
    @SerialName("StudyYear") val studyYear: Int,
    @SerialName("EnabledModules") val enabledModules: List<JsonObject>,
    @SerialName("SettingModules") val settingModules: JsonObject

//todo
): IBakaResult {

}
