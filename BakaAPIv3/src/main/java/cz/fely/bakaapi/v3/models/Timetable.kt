package cz.fely.bakaapi.v3.models

import cz.fely.bakaapi.v3.ext.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonArraySerializer
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonObjectSerializer
import java.util.*

@Serializable
data class Timetable(
    @SerialName("Hours") val hours: List<Hour>,
     @SerialName("Days") val days: List<Day>,
     @SerialName("Classes") val classes: List<Class>,
     @SerialName("Groups") val groups: List<Group>,
     @SerialName("Subjects") val subjects: List<Subject>,
     @SerialName("Teachers") val teachers: List<Teacher>,
     @SerialName("Rooms") val rooms: List<Room>,
     @SerialName("Cycles") val cycles: List<Cycle>
): IBakaResult {
    @Serializable
    data class Hour(
        @SerialName("Id") val id: Int,
        @SerialName("Caption") val caption: String,
        @SerialName("BeginTime") val beginTime: String,
        @SerialName("EndTime") val endTime: String
    )

    @Serializable
    data class Day(
        @Serializable(JsonArraySerializer::class) @SerialName("Atoms") val atoms: List<Atom>,
        @SerialName("DayOfWeek") val dayOfWeek: Int,
        @Serializable(DateSerializer::class) @SerialName("Date") val date: Date,
        @SerialName("DayDescription") val dateDescription: String,
        @SerialName("DayType") val dayType: String
    ) {
        @Serializable
        data class Atom(
            @SerialName("HourId") val hourId: Int,
            @Serializable(JsonArraySerializer::class) @SerialName("GroupsIds") val groupIds: List<String>,
            @SerialName("SubjectId") val subjectId: String,
            @SerialName("TeacherId") val teacherId: String,
            @SerialName("RoomId") val roomId: String,
            @Serializable(JsonArraySerializer::class) @SerialName("CycleIds") val cycleIds: List<String>,
            @SerialName("Change") val change: Change? = null,
            @Serializable(JsonArraySerializer::class) @SerialName("HomeworkIds") val homeworkIds: List<String>,
            @SerialName("Theme") val theme: String
        )
    }

    @Serializable
    data class Class(
        @SerialName("Id") val id: String,
        @SerialName("Abbrev") val abbreviation: String,
        @SerialName("Name") val name: String
    )

    @Serializable
    data class Group(
        @SerialName("ClassId") val classId: String,
        @SerialName("Id") val id: String,
        @SerialName("Abbrev") val abbreviation: String,
        @SerialName("Name") val name: String
    )

    @Serializable
    data class Subject(
        @SerialName("Id") val id: String,
        @SerialName("Abbrev") val abbreviation: String,
        @SerialName("Name") val name: String
    )

    @Serializable
    data class Teacher(
        @SerialName("Id") val id: String,
        @SerialName("Abbrev") val abbreviation: String,
        @SerialName("Name") val name: String
    )

    @Serializable
    data class Room(
        @SerialName("Id") val id: String,
        @SerialName("Abbrev") val abbreviation: String,
        @SerialName("Name") val name: String
    )

    @Serializable
    data class Cycle(
        @SerialName("Id") val id: String,
        @SerialName("Abbrev") val abbreviation: String,
        @SerialName("Name") val name: String
    )
}