package cz.fely.bakaapi.v3.models

import cz.fely.bakaapi.v3.ext.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class MarkMeasure(
    @SerialName("PedagogicalMeasures") val pedagogicalMeasures: List<PedagogicalMeasure>
): IBakaResult {
    @Serializable
    data class PedagogicalMeasure(
        @SerialName("SchoolYear") val schoolYear: String,
        @SerialName("Semester") val semester: String,
        @SerialName("TypeLabel") val typeLabel: String,
        @Serializable(DateSerializer::class) @SerialName("Date") val date: Date,
        @SerialName("TypeId") val typeId: String,
        @SerialName("Text") val text: String
    )
}