package cz.fely.bakaapi.v3.models

import com.github.kittinunf.result.Result
import java.lang.Exception

sealed class BakaResult<out V: IBakaResult, out E: Exception> {
    open operator fun component1(): V? = null
    open operator fun component2(): E? = null

    inline fun <X> fold(success: (V) -> X, failure: (E, V?) -> X): X = when (this) {
        is Success -> success(this.value)
        is Failure -> failure(this.error, this.value)
    }

    inline fun fold(success: (V) -> Unit, failure: (E, V?) -> Unit): Unit = when(this) {
        is Success -> success(this.value)
        is Failure -> failure(this.error, this.value)
    }


    abstract fun get(): V

    class Success<out V : IBakaResult>(val value: V) : BakaResult<V, Nothing>() {
        override fun component1(): V? = value

        override fun get(): V = value

        override fun toString() = "[Success: $value]"

        override fun hashCode(): Int = value.hashCode()

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            return other is Success<*> && value == other.value
        }
    }

    class Failure<out V : IBakaResult, out E : Exception>(val value: V?, val error: E) : BakaResult<V, E>() {
        override fun component2(): E? = error

        override fun get() = throw error

        fun getException(): E = error

        override fun toString() = "[Failure: $error]"

        override fun hashCode(): Int = error.hashCode()

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            return other is Failure<*,*> && error == other.error
        }
    }

    companion object {
        // Factory methods
        fun <E : Exception> error(ex: E) = Result.Failure(ex)

        fun <V : IBakaResult> success(v: V) = Result.Success(v)

        fun <V : IBakaResult> of(value: V?, fail: (() -> Exception) = { Exception() }): Result<V, Exception> =
            value?.let { success(it) } ?: error(fail())

        fun <V : IBakaResult, E: Exception> of(f: () -> V): Result<V, E> = try {
            success(f())
        } catch (ex: Exception) {
            error(ex as E)
        }
    }
}