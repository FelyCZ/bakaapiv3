package cz.fely.bakaapi.v3.models

import cz.fely.bakaapi.v3.ext.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class FinalMarks(
    @SerialName("CertificateTerms") val certificateTerms: List<CertificateTerm>
): IBakaResult {
    @Serializable
    data class CertificateTerm(
        @SerialName("FinalMarks") val finalMarks: List<FinalMark>,
        @SerialName("Subjects") val subjects: List<SubjectInfo>,
        @SerialName("GradeName") val gradeName: String,
        @SerialName("Grade") val grade: Int,
        @SerialName("YearInSchool") val yearInSchool: Int,
        @SerialName("SchoolYear") val schoolYear: String,
        @SerialName("Semester") val semester: String,
        @SerialName("SemesterName") val semesterName: String,
        @SerialName("Repeated") val repeated: Boolean,
        @SerialName("Closed") val closed: Boolean,
        @SerialName("AchievementText") val achievementText: String,
        @SerialName("MarksAverage") val marksAverage: Double,
        @SerialName("AbsentHours") val absentHours: Int,
        @SerialName("NotExcusedHours") val notExcusedHours: Int,
        @Serializable(DateSerializer::class) @SerialName("CertificateDate") val certificateDate: Date
        ) {
        @Serializable
        data class  FinalMark(
            @Serializable(DateSerializer::class) @SerialName("MarkDate") val markDate: Date,
            @Serializable(DateSerializer::class) @SerialName("EditDate") val editDate: Date,
            @SerialName("MarkText") val markText: String,
            @SerialName("SubjectId") val subjectId: String,
            @SerialName("Id") val id: String
        )
    }
}