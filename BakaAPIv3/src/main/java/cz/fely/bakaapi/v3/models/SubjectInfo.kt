package cz.fely.bakaapi.v3.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SubjectInfo(
    @SerialName("Id") val id: String,
    @SerialName("Abbrev") val abbreviation: String,
    @SerialName("Name") val name: String
)