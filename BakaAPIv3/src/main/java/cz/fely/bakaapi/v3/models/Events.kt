package cz.fely.bakaapi.v3.models

import cz.fely.bakaapi.v3.ext.DateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class Events(
    @SerialName("Events") val events: List<Event>
): IBakaResult {
    @Serializable
    data class Event(
        @SerialName("Id") val id: String,
        @SerialName("Title") val title: String,
        @SerialName("Description") val description: String,
        @SerialName("Times") val times: List<Time>,
        @SerialName("EventType") val eventType: EventType,
        @SerialName("Classes") val classes: String
    ) {
        @Serializable
        data class Time(
            @SerialName("WholeDay") val wholeDay: Boolean,
            @Serializable(DateSerializer::class) @SerialName("StartTime") val startTime: Date,
            @Serializable(DateSerializer::class) @SerialName("EndTime") val endTime: Date
        )

        @Serializable
        data class EventType(
            @SerialName("Id") val id: String,
            @SerialName("Abbrev") val abbreviation: String,
            @SerialName("Name") val name: String
        )
    }
}