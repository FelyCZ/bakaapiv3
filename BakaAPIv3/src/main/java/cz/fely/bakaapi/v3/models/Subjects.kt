package cz.fely.bakaapi.v3.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Subjects(
    @SerialName("Subjects") val subjects: List<Subject>
): IBakaResult {
    @Serializable
    data class Subject(
        @SerialName("SubjectID") val subjectId: String,
        @SerialName("SubjectName") val subjectName: String,
        @SerialName("SubjectAbbrev") val subjectAbbreviation: String,
        @SerialName("TeacherID") val teacherId: String,
        @SerialName("TeacherName") val teacherName: String,
        @SerialName("TeacherAbbrev") val teacherAbbreviation: String,
        @SerialName("TeacherEmail") val teacherEmail: String,
        @SerialName("TeacherWeb") val teacherWeb: String? = null,
        @SerialName("TeacherSchoolPhone") val teacherSchoolPhone: String? = null,
        @SerialName("TeacherHomePhone") val teacherHomePhone: String? = null,
        @SerialName("TeacherMobilePhone") val teacherMobilePhone: String? = null
    )
}